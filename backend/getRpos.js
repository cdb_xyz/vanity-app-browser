const fs = require("fs");
const path = require("path");
const { rootAppPath } = require("./appServeConfig.json");

const getRepos = () => {
    const repos = fs.readdirSync(rootAppPath).map(r => {
        return { repoName: r, repoPath: path.join(rootAppPath, r) };
    });
    return repos;
};

module.exports = getRepos;