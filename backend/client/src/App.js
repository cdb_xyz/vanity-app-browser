import React, { Component } from "react";
import "./App.css";

class App extends Component {
    state = {
        apps: []
    }

    componentDidMount = async () => {
        const results = await fetch("/api/get-apps");
        const apps = await results.json();
        this.setState({ apps });
    }

    render() {
        return (
            <div className="App" style={{ width: "80%", maxWidth: "800px", margin: "0 auto", marginTop:"0", fontFamily: "'Roboto', sans-serif", color: "#F5F5F5" }}>
                <header>
                    <h1>Apps</h1>
                </header>
                <hr/>
                {this.state.apps.map(a => {
                    return (
                        <React.Fragment>
                            <p style={{ marginBottom: "5em" }}>
                                <h2>{a.repoName}</h2>
                                <p style={{ fontStyle: "italic" }}>{`"${a.description || ""}"`}</p>
                                <a href={`/${a.repoName}`} style={{ color: "#F5F5F5" }}>Launch</a>
                            </p>
                        </React.Fragment>
                    );
                })}
            </div>
        );
    }
}

export default App;
