const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const getRepos = require("./getRpos");

const apiRouter = require("./routes/api");
const app = express();



app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/", express.static(path.join(__dirname, "client", "build")));
app.use("/api", apiRouter);

getRepos().forEach(r => {
    const repoRoute = express.static(path.join(r.repoPath, "client", "build"));
    app.use(`/${r.repoName}`, repoRoute);
    app.use(`/${r.repoName}*`, repoRoute);
});

module.exports = app;
