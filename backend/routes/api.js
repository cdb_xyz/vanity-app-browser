const express = require("express");
const router = express.Router();
const getRepos = require("../getRpos");
const path = require("path");


router.get("/get-apps", function (req, res) {

    const apps = getRepos().map(r => {
        const { repoName } = r;
        const jsonPath = path.join(r.repoPath, "client", "package.json");
        const { description } = require(jsonPath);
        return { repoName, description };
    }); 
    res.json(apps);
});

module.exports = router;
